Title: Was ist OpenSchoolMaps?
Date: 2018-07-11
Category: OpenSchoolMaps
Tags: meta, mission statement
Status: hidden
Url:
Save_as: index.html

![]({filename}/images/schoolkids.jpg)

OpenSchoolMaps.ch
ist ein Projekt zur Förderung
offener Karten im Unterricht,
sei es
in digitaler Form,
im Browser,
auf mobilen Geräten
oder
in gedruckter Form.
Dabei werden vornehmlich
freie Daten von OpenStreetMap
eingesetzt.

Angesprochen sind vor allem Lehrpersonen auf Sekundar- und Gymnasialstufe.

Sowohl
die Karten und Karten-Daten
als auch
die Lehr- und Lernmaterialien dazu
werden
als Open Educational Resource
[OER](https://de.wikipedia.org/wiki/Open_Educational_Resources)
bereitgestellt,
so dass jeder
zu ihnen beitragen,
sie frei (auch ausserhalb des Unterrichts!) nutzen
und
weitergeben
kann und darf.
